#!/usr/bin/env python
# -*- coding: utf8 -*-

"""A vocable editation dialog for the Quettar Lambion application"""

import wx
from . import ci
from .languages import languages

class DlgVocableEdit(wx.Dialog):
    """DlgUpravySlovicka(parent: wxWindow, jazyk: unicode, slovicko: wxListItem): \
    výsledek modálního dialogu

    Ukáže modální dialogové okénko pro editaci slovíčka
    """

    def __init__(self, parent, language, vocable=None):
        if vocable is None:
            title=u"New vocable"
        else:
            title=u'Vocable "%s"'%(vocable[0])

        wx.Dialog.__init__(self, parent, -1, title)

        self.vocable = vocable
        self.encoding_module = languages.get(language)

        szr_all=self.szr_all=wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(szr_all)

        ########################################################################

        fnt_native=wx.FFont(14,wx.FONTFAMILY_ROMAN)
        fnt_translation = self.make_font()

        self.szr_voc=wx.BoxSizer(wx.VERTICAL)

        pan_foreign = self.pan_foreign=wx.StaticBox(self)
        pan_foreign.SetLabel(u"&Foreign")
        szr_foreign = self.szr_foreign = \
            wx.StaticBoxSizer(pan_foreign, wx.VERTICAL)
        ed_transcription = self.ed_transcription = wx.TextCtrl(self)
        ed_transcription.SetFont(fnt_native)
        ed_transcription.AutoSize = True
        szr_foreign.Add(ed_transcription, 0, wx.GROW | wx.ALL, 8)

        if hasattr(self.encoding_module,'direction'):
            align = self.encoding_module.direction
        else:
            align = wx.TE_LEFT
        ed_translation=self.ed_translation=wx.TextCtrl(
            self,
            style = align | wx.TE_READONLY
            )
        ed_translation.SetFont(fnt_translation)
        ed_translation.AutoSize = True
        szr_foreign.Add(ed_translation, 0, wx.GROW | wx.ALL, 8)

        ed_transcription.Bind(
            wx.EVT_TEXT, self.trancription_change, ed_transcription
        )

        szr_all.Add(szr_foreign, 0, wx.GROW | wx.LEFT | wx.TOP | wx.RIGHT, 8)

        pan_native = self.pan_native = wx.StaticBox(self)
        pan_native.SetLabel(u"&Native")
        szr_native=self.szr_native=wx.StaticBoxSizer(pan_native, wx.VERTICAL)
        szr_all.Add(szr_native, 0, wx.GROW | wx.LEFT | wx.TOP | wx.RIGHT, 8)
        ed_native = self.ed_native = wx.TextCtrl(self)
        ed_native.SetFont(fnt_native)
        szr_native.Add(ed_native, 1, wx.GROW | wx.ALL, 8)

        chb_test = self.chb_test=wx.CheckBox(
            self, label= "&Test this vocable", style=wx.CHB_RIGHT
        )
        szr_all.Add(
            chb_test,
            0,
            wx.ALIGN_CENTER_HORIZONTAL | wx.TOP | wx.LEFT | wx.RIGHT,
            8
        )

        if vocable is not None:
            ed_native.SetValue(vocable[0])
            # We are displaying the foreign word transcription
            # and right after that the word gets compiled
            # into the foreign language
            ed_transcription.SetValue(vocable[1][1])
            chb_test.SetValue(vocable[1][0])

        szr_btns = self.szr_btn = wx.StdDialogButtonSizer()
        szr_all.Add(szr_btns, 0, wx.GROW | wx.ALL, 8)

        # The OK button
        btn_ok = self.btn_ok = wx.Button(parent=self, id=wx.ID_OK)
        szr_btns.Add(btn_ok)
        szr_btns.SetAffirmativeButton(btn_ok)
        btn_ok.Bind(wx.EVT_BUTTON, self.ok_click)

        # The Cancel button:
        btn_cancel = self.btn_cancel = wx.Button(parent=self, id=wx.ID_CANCEL)
        szr_btns.Add(btn_cancel, wx.GROW | wx.ALL, 8)
        szr_btns.SetCancelButton(btn_cancel)
        btn_cancel.Label = "&Cancel"

        btn_ok.SetDefault()

        szr_btns.Fit(self)

        # Focus on ed_transcription:
        ed_transcription.SetFocus()

        ########################################################################
        szr_all.Fit(self)

    def make_font(self):
        """Constructs a font from a property dictionary"""
        fnt=None
        if self.encoding_module:
            if hasattr(self.encoding_module,'font'):
                fdict=self.encoding_module.font
                fnt = ci.make_font(fdict)
        if not fnt:
            fnt = wx.FFont(14, wx.FONTFAMILY_ROMAN)
        return fnt

    def trancription_change(self, evt):
        """The ed_transcription change event handler"""
        if self.encoding_module:
            if hasattr(self.encoding_module, 'compile_transcription'):
                transcription=self.encoding_module.compile_transcription(
                    self.ed_transcription.Value
                )
            else:
                transcription=self.ed_transcription.Value
        else:
            transcription=self.ed_transcription.Value
        self.ed_translation.SetValue(transcription)
        evt.Skip()

    def ok_click(self, evt):
        """The OK button click event handler"""
        native = self.ed_native.GetValue()
        transcription = self.ed_transcription.GetValue()
        if native == '' and transcription == '':
            self.vocable = None
            self.SetReturnCode(wx.CANCEL)
        else:
            test = bool(self.chb_test.GetValue())
            self.vocable = (native, (test, transcription))
            self.SetReturnCode(wx.OK)
        if not self.IsModal:
            self.Close()
        evt.Skip()

if __name__ == '__main__':
    class TestApp(wx.App):
        """A helper class to test out the edit dialog"""
        def OnInit(self): # pylint: disable=invalid-name
            """The application initialization"""
            dlg = DlgVocableEdit(None, "Hebrew")
            self.SetTopWindow(dlg)
            dlg.Show(True)
            return True


    app = TestApp()
    app.MainLoop()
