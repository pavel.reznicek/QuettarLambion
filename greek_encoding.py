# -*- coding:utf8 -*-

"""The Greek encoding for the Quettar Lambion application."""

import wx
#from quettar_lambion.sp import *

SYMBOL_CODES = {
    # capital letters
    'A':        0x0391,
    'A|':       0x1FBC,
    'B':        0x0392,
    'G':        0x0393,
    'D':        0x0394,
    'E':        0x0395,
    'Z':        0x0396,
    'É':        0x0397,
    'É|':       0x1FCC,
    'H':        0x0397,
    'H|':       0x1FCC,
    'TH':       0x0398,
    'Th':       0x0398,
    'I':        0x0399,
    'K':        0x039A,
    'L':        0x039B,
    'M':        0x039C,
    'N':        0x039D,
    'X':        0x039E,
    'O':        0x039F,
    'P':        0x03A0,
    'R':        0x03A1,
    'C':        0x03A2,
    'S':        0x03A3,
    'T':        0x03A4,
    'U':        0x03A5,
    'Y':        0x03A5,
    'F':        0x03A6,
    'PH':       0x03A6,
    'Ph':       0x03A6,
    'CH':       0x03A7,
    'Ch':       0x03A7,
    'PS':       0x03A8,
    'Ps':       0x03A8,
    'W':        0x03A9,
    'W|':       0x1FFC,
    'Ó':        0x03A9,
    'Ó|':       0x1FFC,
    # minuscle letters
    'a':        0x03B1,
    'a|':       0x1FB3,
    'b':        0x03B2,
    'g':        0x03B3,
    'd':        0x03B4,
    'e':        0x03B5,
    'z':        0x03B6,
    'h':        0x03B7,
    'h|':       0x1FC3,
    'é':        0x03B7,
    'é|':       0x1FC3,
    'th':       0x03B8,
    'i':        0x03B9,
    'k':        0x03BA,
    'l':        0x03BB,
    'm':        0x03BC,
    'n':        0x03BD,
    'x':        0x03BE,
    'o':        0x03BF,
    'p':        0x03C0,
    'r':        0x03C1,
    'c':        0x03C2,
    's':        0x03C3,
    't':        0x03C4,
    'u':        0x03C5,
    'y':        0x03C5,
    'f':        0x03C6,
    'ph':       0x03C6,
    'ch':       0x03C7,
    'ps':       0x03C8,
    'w':        0x03C9,
    'w|':       0x1FF3,
    'ó':        0x03C9,
    'ó|':       0x1FF3,
    # variants
    r'\th/':    0x03D1,
    r'\U/':     0x03D2,
    r'\Y/':     0x03D3,
    r'\f/':     0x03D5,
    r'\ph/':    0x03D5,
    r'\w/':     0x03D6,
    r'\ó/':     0x03D6,
    r'\kk/':    0x03D7,
    r'\k/':     0x03F0,
    r'\r':      0x03F1,
    r'\c/':     0x03F2,
    'j':        0x03F3,
    r'\TH/':    0x03F4,
    r'\e/':     0x03F5,
    r'\-e/':    0x03F6,
    # koppa
    'Q':        0x03D8,
    'q':        0x03D9,
    # vau
    'V':        0x03DC,
    'v':        0x03DD,
    # diacritical marks
    '||':       0x1FBE, # standalone iota subscriptum
    # letters with diacritics:
    # (nothing)
    # interpunction
    '?':        0x037E,
    r'\?/':     ord(u'?'),
    r'\|/':     ord(u'|'),
    r'\(/':     ord(u'('),
    r'\)/':     ord(u')'),
    ':':        0x0387,
    ' ':        0x0020 # space – must be in the list too
                       # (it binds with standalone marks)
}

ORDERED_SYMBOL_CODES = sorted(SYMBOL_CODES.keys())
ORDERED_SYMBOL_CODES.reverse()

VOCALS = 'aeéhioyuów'
DIAKRITHONTES = VOCALS + 'r'

BASE_VOCAL_CODES = {
    'a':    0x1F00,
    'A':    0x1F08,
    'e':    0x1F10,
    'E':    0x1F18,
    'é':    0x1F20,
    'É':    0x1F28,
    'i':    0x1F30,
    'I':    0x1F38,
    'o':    0x1F40,
    'O':    0x1F48,
    'y':    0x1F50,
    'Y':    0x1F58,
    'ó':    0x1F60,
    'Ó':    0x1F68,
    'a|':   0x1F80,
    'A|':   0x1F88,
    'é|':   0x1F90,
    'É|':   0x1F98,
    'ó|':   0x1FA0,
    'Ó|':   0x1FA8
}

UNAMBIGUOUS_LETTERS={
    'H':    'É',
    'H|':   'É|',
    'h':    'é',
    'h|':   'é|',
    'TH':   'Th',
    'th':   'th',
    'U':    'Y',
    'u':    'y',
    'PH':   'F',
    'Ph':   'f',
    'ph':   'f',
    'CH':   'Ch',
    'PS':   'Ps',
    'W':    'Ó',
    'W|':   'Ó|',
    'w':    'ó',
    'w|':   'ó|'
}

DIA_SPIRITUS_LENIS        =   1
DIA_SPIRITUS_ASPER        =   2
DIA_ACCENTUS_GRAVIS       =   4
DIA_ACCENTUS_ACUTUS       =   8
DIA_ACCENTUS_CIRCUMFLEXUS =  16
DIA_LITERA_CAPITALIS      =  32
DIA_DIAIRESIS             =  64
DIA_BREVIS                = 128
DIA_MAKRON                = 512

DIA_LENIS_GRAVIS = DIA_SPIRITUS_LENIS | DIA_ACCENTUS_GRAVIS
DIA_ASPER_GRAVIS = DIA_SPIRITUS_ASPER | DIA_ACCENTUS_GRAVIS
DIA_LENIS_ACUTUS = DIA_SPIRITUS_LENIS | DIA_ACCENTUS_ACUTUS
DIA_ASPER_ACUTUS = DIA_SPIRITUS_ASPER | DIA_ACCENTUS_ACUTUS
DIA_LENIS_CIRCUMFLEXUS = DIA_SPIRITUS_LENIS | DIA_ACCENTUS_CIRCUMFLEXUS
DIA_ASPER_CIRCUMFLEXUS = DIA_SPIRITUS_ASPER | DIA_ACCENTUS_CIRCUMFLEXUS

DIAKRITHON_CODE_SHIFT = {
    DIA_SPIRITUS_LENIS:     0,
    DIA_SPIRITUS_ASPER:     1,
    DIA_LENIS_GRAVIS:       2,
    DIA_ASPER_GRAVIS:       3,
    DIA_LENIS_ACUTUS:       4,
    DIA_ASPER_ACUTUS:       5,
    DIA_LENIS_CIRCUMFLEXUS: 6,
    DIA_ASPER_CIRCUMFLEXUS: 7
}

MARKS = {
    '’': DIA_SPIRITUS_LENIS,
    ')': DIA_SPIRITUS_LENIS,
    '>': DIA_SPIRITUS_LENIS,
    '‘': DIA_SPIRITUS_ASPER,
    '(': DIA_SPIRITUS_ASPER,
    '<': DIA_SPIRITUS_ASPER,
    '`': DIA_ACCENTUS_GRAVIS,
    "'": DIA_ACCENTUS_ACUTUS,
    '´': DIA_ACCENTUS_ACUTUS,
    '^': DIA_ACCENTUS_CIRCUMFLEXUS,
    '~': DIA_ACCENTUS_CIRCUMFLEXUS,
    '"': DIA_DIAIRESIS,
    '¨': DIA_DIAIRESIS,
    '˘': DIA_BREVIS,
    '_': DIA_MAKRON
}

NAMED_CHARACTERS = {
    # accents
    (' ', DIA_ACCENTUS_GRAVIS):                         0x1FEF,
    (' ', DIA_ACCENTUS_ACUTUS):                         0x1FFD,
    (' ', DIA_ACCENTUS_CIRCUMFLEXUS):                   0x1FC0,
    # spiritus lenis
    (' ', DIA_SPIRITUS_LENIS):                          0x1FBD,
    (' ', DIA_LENIS_GRAVIS):                            0x1FCD,
    (' ', DIA_LENIS_ACUTUS):                            0x1FCE,
    (' ', DIA_LENIS_CIRCUMFLEXUS):                      0x1FCF,
    # spiritus asper
    (' ', DIA_SPIRITUS_ASPER):                          0x1FFE,
    (' ', DIA_ASPER_GRAVIS):                            0x1FDD,
    (' ', DIA_ASPER_ACUTUS):                            0x1FCE,
    (' ', DIA_ASPER_CIRCUMFLEXUS):                      0x1FCF,
    # diairesis
    (' ', DIA_DIAIRESIS):                               0x0307,
    (' ', DIA_ACCENTUS_GRAVIS | DIA_DIAIRESIS):         0x1FED,
    (' ', DIA_ACCENTUS_ACUTUS | DIA_DIAIRESIS):         0x1FEE,
    (' ', DIA_ACCENTUS_CIRCUMFLEXUS | DIA_DIAIRESIS):   0x1FC1,
    # graves et acutes
    ('a', DIA_ACCENTUS_GRAVIS):                         0x1F70,
    ('a', DIA_ACCENTUS_ACUTUS):                         0x1F71,
    ('e', DIA_ACCENTUS_GRAVIS):                         0x1F72,
    ('e', DIA_ACCENTUS_ACUTUS):                         0x1F73,
    ('é', DIA_ACCENTUS_GRAVIS):                         0x1F74,
    ('é', DIA_ACCENTUS_ACUTUS):                         0x1F75,
    ('i', DIA_ACCENTUS_GRAVIS):                         0x1F76,
    ('i', DIA_ACCENTUS_ACUTUS):                         0x1F77,
    ('o', DIA_ACCENTUS_GRAVIS):                         0x1F78,
    ('o', DIA_ACCENTUS_ACUTUS):                         0x1F79,
    ('y', DIA_ACCENTUS_GRAVIS):                         0x1F7A,
    ('y', DIA_ACCENTUS_ACUTUS):                         0x1F7B,
    ('ó', DIA_ACCENTUS_GRAVIS):                         0x1F7C,
    ('ó', DIA_ACCENTUS_ACUTUS):                         0x1F7D,

    (u'a', DIA_BREVIS):                                 0x1FB0,
    (u'a', DIA_MAKRON):                                 0x1FB1,
    (u'a|', DIA_ACCENTUS_GRAVIS):                       0x1FB2,
    (u'a|', DIA_ACCENTUS_ACUTUS):                       0x1FB4,
    (u'a', DIA_ACCENTUS_CIRCUMFLEXUS):                  0x1FB6,
    (u'a|', DIA_ACCENTUS_CIRCUMFLEXUS):                 0x1FB7,
    (u'A', DIA_BREVIS):                                 0x1FB8,
    (u'A', DIA_MAKRON):                                 0x1FB9,
    (u'A', DIA_ACCENTUS_GRAVIS):                        0x1FBA,
    (u'A', DIA_ACCENTUS_ACUTUS):                        0x1FBB,

    (u'é|', DIA_ACCENTUS_GRAVIS):                       0x1FC2,
    (u'é|', DIA_ACCENTUS_ACUTUS):                       0x1FC4,
    (u'é', DIA_ACCENTUS_CIRCUMFLEXUS):                  0x1FC6,
    (u'é|', DIA_ACCENTUS_CIRCUMFLEXUS):                 0x1FC7,
    (u'E', DIA_ACCENTUS_GRAVIS):                        0x1FC8,
    (u'E', DIA_ACCENTUS_ACUTUS):                        0x1FC9,
    (u'É', DIA_ACCENTUS_GRAVIS):                        0x1FCA,
    (u'É', DIA_ACCENTUS_ACUTUS):                        0x1FCB,

    (u'i', DIA_BREVIS):                                 0x1FD0,
    (u'i', DIA_MAKRON):                                 0x1FD1,
    (u'i', DIA_ACCENTUS_GRAVIS | DIA_DIAIRESIS):        0x1FD2,
    (u'i', DIA_ACCENTUS_ACUTUS | DIA_DIAIRESIS):        0x1FD3,
    (u'i', DIA_ACCENTUS_CIRCUMFLEXUS):                  0x1FD6,
    (u'i', DIA_ACCENTUS_CIRCUMFLEXUS | DIA_DIAIRESIS):  0x1FD7,
    (u'I', DIA_BREVIS):                                 0x1FD8,
    (u'I', DIA_MAKRON):                                 0x1FD9,
    (u'I', DIA_ACCENTUS_GRAVIS):                        0x1FDA,
    (u'I', DIA_ACCENTUS_ACUTUS):                        0x1FDB,

    (u'y', DIA_BREVIS):                                 0x1FE0,
    (u'y', DIA_MAKRON):                                 0x1FE1,
    (u'y', DIA_ACCENTUS_GRAVIS | DIA_DIAIRESIS):        0x1FE2,
    (u'y', DIA_ACCENTUS_ACUTUS | DIA_DIAIRESIS):        0x1F23,
    (u'r', DIA_SPIRITUS_LENIS):                         0x1FE4,
    (u'r', DIA_SPIRITUS_ASPER):                         0x1FE5,
    (u'y', DIA_ACCENTUS_CIRCUMFLEXUS):                  0x1FE6,
    (u'y', DIA_ACCENTUS_CIRCUMFLEXUS | DIA_DIAIRESIS):  0x1FE7,
    (u'Y', DIA_BREVIS):                                 0x1FE8,
    (u'Y', DIA_MAKRON):                                 0x1FE9,
    (u'Y', DIA_ACCENTUS_GRAVIS):                        0x1FEA,
    (u'Y', DIA_ACCENTUS_ACUTUS):                        0x1FEB,
    (u'R', DIA_SPIRITUS_ASPER):                         0x1FEC,

    (u'ó|', DIA_ACCENTUS_GRAVIS):                       0x1FF2,
    (u'ó|', DIA_ACCENTUS_ACUTUS):                       0x1FF4,
    (u'ó', DIA_ACCENTUS_CIRCUMFLEXUS):                  0x1FF6,
    (u'ó|', DIA_ACCENTUS_CIRCUMFLEXUS):                 0x1FF7,
    (u'O', DIA_ACCENTUS_GRAVIS):                        0x1FF8,
    (u'O', DIA_ACCENTUS_ACUTUS):                        0x1FF9,
    (u'Ó', DIA_ACCENTUS_GRAVIS):                        0x1FFA,
    (u'Ó', DIA_ACCENTUS_ACUTUS):                        0x1FFB
}

def get_unambiguous_token(transcription):
    """Returns the unambiguous letter representation
    for the transcribed letter given"""
    result = UNAMBIGUOUS_LETTERS.get(transcription, transcription)
    return result

def get_symbol_code(transcription):
    """Returns the Greek symbol for the transcription given"""
    result = SYMBOL_CODES.get(transcription, transcription)
    return result

def eat_marks(text):
    """Strips the known diacritical marks from the beginning of the text
    and returns a tuple
    containing their combined numeric representation
    and the rest of the text"""
    result = 0
    search_further=True
    while search_further:
        search_further=False
        for token, mark in MARKS.items():
            if text.startswith(token):
                result |= mark
                text = text[len(token):]
                search_further = True
                break
    return (result, text)

def get_letter_with_diacritics_code(letter, dia_marks):
    """Returns the character code of the given letter
    decorated with the diacritical marks given as a numeric representation"""
    unambiguous_token = get_unambiguous_token(letter)
    if (dia_marks in DIAKRITHON_CODE_SHIFT.keys()) and \
    (unambiguous_token in BASE_VOCAL_CODES):
        code = BASE_VOCAL_CODES[unambiguous_token] + \
            DIAKRITHON_CODE_SHIFT[dia_marks]
    elif dia_marks > 0:
        code = NAMED_CHARACTERS.get(
            (unambiguous_token, dia_marks),
            SYMBOL_CODES.get(unambiguous_token, unambiguous_token)
        )
    else:
        code = SYMBOL_CODES.get(unambiguous_token, unambiguous_token)
    return code

def greek(transcription):
    """Returns the Greek text compiled from the given transcription"""
    result = ''
    while transcription:
        dia_marks, transcription = eat_marks(transcription)
        for token in ORDERED_SYMBOL_CODES:
            if transcription.startswith(token):
                unambiguous_token = get_unambiguous_token(token)
                if dia_marks == 0:
                    symbol_code = get_symbol_code(token)
                else:
                    symbol_code = get_letter_with_diacritics_code(
                        unambiguous_token,
                        dia_marks
                    )
                result += chr(symbol_code)
                # eat the recognized token
                transcription = transcription[len(token):]
                break
        else: # the transcription doesn’t start with anything decipherable
            # Add the first character as is
            result += transcription[:1]
            transcription = transcription[1:]
    return result

# The following items should every encoding module define

def compile_transcription(transcription):
    """Compiles the given transcribed text into the original language"""
    result = greek(transcription)
    return result

font = {
    "face":     "serif",
    "size":     14,
    "encoding": wx.FONTENCODING_DEFAULT
}

del wx
