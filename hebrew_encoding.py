# -*- coding:utf8 -*-

"""The Hebrew encoding module for the Quettar Lambion application"""

import wx
from . import ci
from . import unicode_hebrew

CONSONANTS = {
    "álef"              :")",
    "bét"               :"b",
    "gímel"             :"g",
    "dálet"             :"d",
    "hé"                :"h",
    "wáw"               :"w",
    "zajin"             :"z",
    "chét"              :"x",
    "tét"               :"T",
    "jód"               :"j",
    "kaf"               :"k",
    "lámed"             :"l",
    "mém"               :"m",
    "nún"               :"n",
    "sámek"             :"s",
    "ajin"              :"(",
    "pé"                :"p",
    "cáde"              :"c",
    "qóf"               :"q",
    "réš"               :"r",
    "sín"               :"ś",
    "šín"               :"š",
    "sínšín"            :"S",
    "táw"               :"t",
    "mater lectionis"   :chr(31)
}

MARKS = {
    "sillúq"        : r"\:/", # must come before ":"
    "sóf pásúq"     : r"\:/",
    "dageš"         : ".",
    "šewa"          : ":",
    "chatef patach" : "a:", # must come before "a" because it is longer
    "patach"        : "a",
    "chatef qámec"  : "o:",
    "qámec"         : "á",
    "chatef segól"  : "e:",
    "segól"         : "e",
    "céré"          : "é",
    "chíreq"        : "i",
    "chólem gadól"  : "ó",
    "chólem"        : "o",
    "šúreq"         : "ú",
    "qibbúc"        : "u",
    "sofít"         : " ",
    "meteg"         : "|",
    "atnách"        : "^",
    "maqef"         : "-",
    "apostrophe"      : "'",
    "unknown mark"  : chr(31)
}

CONSONANT_NAMES = ci.swap_dict(CONSONANTS)
MARK_NAMES      = ci.swap_dict(MARKS)

WIDTHS = CONSONANTS.copy()

CONSONANT_WIDTH_NARROW  = 0
CONSONANT_WIDTH_NORMAL  = 1
CONSONANT_WIDTH_QOF     = 2
CONSONANT_WIDTH_LAMED   = 3
WIDTHS.update(
{
    "álef"      : CONSONANT_WIDTH_NORMAL,
    "bét"       : CONSONANT_WIDTH_NORMAL,
    "gímel"     : CONSONANT_WIDTH_NARROW,
    "dálet"     : CONSONANT_WIDTH_NORMAL,
    "hé"        : CONSONANT_WIDTH_NORMAL,
    "wáw"       : CONSONANT_WIDTH_NARROW,
    "zajin"     : CONSONANT_WIDTH_NARROW,
    "chét"      : CONSONANT_WIDTH_NORMAL,
    "tét"       : CONSONANT_WIDTH_NORMAL,
    "jód"       : CONSONANT_WIDTH_NARROW,
    "kaf"       : CONSONANT_WIDTH_NORMAL,
    "lámed"     : CONSONANT_WIDTH_LAMED,
    "mém"       : CONSONANT_WIDTH_NORMAL,
    "nún"       : CONSONANT_WIDTH_NARROW,
    "sámek"     : CONSONANT_WIDTH_NORMAL,
    "ajin"      : CONSONANT_WIDTH_NORMAL,
    "pé"        : CONSONANT_WIDTH_NORMAL,
    "cáde"      : CONSONANT_WIDTH_NORMAL,
    "qóf"       : CONSONANT_WIDTH_QOF,
    "réš"       : CONSONANT_WIDTH_NORMAL,
    "sín"       : CONSONANT_WIDTH_NORMAL,
    "šín"       : CONSONANT_WIDTH_NORMAL,
    "sínšín"    : CONSONANT_WIDTH_NORMAL,
    "táw"       : CONSONANT_WIDTH_NORMAL,
    "mater lectionis"   : CONSONANT_WIDTH_NORMAL
}
)

MARK_PROP_NONE                  =  0
MARK_PROP_VOWEL                 =  1
MARK_PROP_BOTTOM                =  2
MARK_PROP_TOP                   =  4
MARK_PROP_STANDALONE            =  8
MARK_PROP_KHATEF                = 16
MARK_PROP_ACCENT                = 32
MARK_PROP_CONS_MODIFIER         = 64
MARK_PROP_BOTTOM_VOWEL          = MARK_PROP_VOWEL | MARK_PROP_BOTTOM
MARK_PROP_TOP_VOWEL             = MARK_PROP_VOWEL | MARK_PROP_TOP
MARK_PROP_STANDALONE_VOWEL      = MARK_PROP_VOWEL | MARK_PROP_STANDALONE
MARK_PROP_BOTTOM_ACCENT         = MARK_PROP_ACCENT | MARK_PROP_BOTTOM
MARK_PROP_TOP_ACCENT            = MARK_PROP_ACCENT | MARK_PROP_TOP
MARK_PROP_STANDALONE_ACCENT     = MARK_PROP_ACCENT | MARK_PROP_STANDALONE


MARKS_PROPS=MARKS.copy()
MARKS_PROPS.update({
    "sillúq"        : MARK_PROP_STANDALONE_ACCENT,
    "sóf pásúq"     : MARK_PROP_STANDALONE_ACCENT,
    "dageš"         : MARK_PROP_CONS_MODIFIER,
    "šewa"          : MARK_PROP_BOTTOM_VOWEL,
    "chatef patach" : MARK_PROP_BOTTOM_VOWEL | MARK_PROP_KHATEF,
    "patach"        : MARK_PROP_BOTTOM_VOWEL,
    "chatef qámec"  : MARK_PROP_BOTTOM_VOWEL | MARK_PROP_KHATEF,
    "qámec"         : MARK_PROP_BOTTOM_VOWEL,
    "chatef segól"  : MARK_PROP_BOTTOM_VOWEL | MARK_PROP_KHATEF,
    "segól"         : MARK_PROP_BOTTOM_VOWEL,
    "céré"          : MARK_PROP_BOTTOM_VOWEL,
    "chíreq"        : MARK_PROP_BOTTOM_VOWEL,
    "chólem"        : MARK_PROP_TOP_VOWEL,
    "chólem gadól"  : MARK_PROP_STANDALONE_VOWEL,
    "qibbúc"        : MARK_PROP_BOTTOM_VOWEL,
    "šúreq"         : MARK_PROP_STANDALONE_VOWEL,
    "sofít"         : MARK_PROP_CONS_MODIFIER,
    "meteg"         : MARK_PROP_BOTTOM_ACCENT,
    "atnách"        : MARK_PROP_BOTTOM_ACCENT,
    "maqef"         : MARK_PROP_STANDALONE_ACCENT,
    "apostrophe"    : MARK_PROP_STANDALONE_ACCENT,
    "unknown mark"  : MARK_PROP_STANDALONE_ACCENT
})

def mark_has_properties(mark, props_sought):
    props = MARKS_PROPS[mark]
    result = (props & props_sought) == props_sought
    return result

def extract_marks_with_property(prop, seq):
    result = [mark for mark in seq if mark_has_properties(mark, prop)]
    return result

BOTTOM_MARKS        = extract_marks_with_property(
    MARK_PROP_BOTTOM,       MARKS.keys()
)
BOTTOM_VOWELS       = extract_marks_with_property(
    MARK_PROP_VOWEL,        BOTTOM_MARKS
)
BOTTOM_ACCENTS      = extract_marks_with_property(
    MARK_PROP_ACCENT,       BOTTOM_MARKS
)

TOP_MARKS           = extract_marks_with_property(
    MARK_PROP_TOP,          MARKS.keys()
)
TOP_VOWELS          = extract_marks_with_property(
    MARK_PROP_VOWEL,        TOP_MARKS
)
TOP_ACCENTS         = extract_marks_with_property(
    MARK_PROP_ACCENT,       TOP_MARKS
)

STANDALONE_MARKS    = extract_marks_with_property(
    MARK_PROP_STANDALONE,   MARKS.keys()
)
STANDALONE_VOWELS   = extract_marks_with_property(
    MARK_PROP_VOWEL,        STANDALONE_MARKS
)
STANDALONE_ACCENTS  = extract_marks_with_property(
    MARK_PROP_ACCENT,       STANDALONE_MARKS
)

ORDERED_MARKS = sorted(MARKS.values())
ORDERED_MARKS.reverse()

def starts_with_a_consonant(transcription):
    return transcription[:1] in CONSONANTS.values()

def starts_with_a_mark(transcription):
    for mark in ORDERED_MARKS:
        if transcription.startswith(mark):
            return True
    return False

def get_leading_mark(transcription):
    for mark in ORDERED_MARKS:
        if transcription.startswith(mark):
            return mark
    return None

def get_hebrew_data(transcription):
    """
    get_hebrew_data(transcription: str) -> str
    Trasforms simply, almost phonetically encoded hebrew
    into an universal data structure that then gets transformed
    through the hebrew() function into an unicode hebrew text.
    """
    result = []
    while transcription != "":
        # eat a consonant
        if starts_with_a_consonant(transcription):
            cons = transcription[:1]
            transcription = transcription[1:]
        else:
            if starts_with_a_mark(transcription):
                cons = CONSONANTS["mater lectionis"]
                # and inquire the transcription further
            else:
                transcription = transcription[1:]
                cons = CONSONANTS["mater lectionis"]
        # eat a mark
        mark_set = set() # initialization
        if starts_with_a_mark(transcription): # starts with a mark
            # Let’s go inquire that mark ana possible followers:
            # We suppose that there may be a mark
            # but only if something remains there.
            mark = get_leading_mark(transcription)
            while mark:
                transcription = transcription[len(mark):] # delete it
                mark_set.add(mark) # and add it
                mark = get_leading_mark(transcription)
        else: # something unidentifiable (maybe an UFO?)
            # go into the next cycle (try if it isn’t a next consonant)
            pass

        result += [(cons, mark_set)]

    return result

def punctated_consonant(group):
    """punctated_consonant(group: Hebrew data)"""
    result = ""
    consonant = group[0] # consonant
    punctation = group[1]  # marks list
    cons_name = CONSONANT_NAMES[consonant]
    width = WIDTHS[cons_name]

    punctation_marks_names=[MARK_NAMES[mark] for mark in punctation]

    dolni_znacky_v_punktaci=\
        extract_marks_with_property(MARK_PROP_BOTTOM,punctation_marks_names)
    dolni_samohlasky_v_punktaci=\
        extract_marks_with_property(MARK_PROP_BOTTOM_VOWEL,punctation_marks_names)
    dolni_akcenty_v_punktaci=\
        extract_marks_with_property(MARK_PROP_BOTTOM_ACCENT,punctation_marks_names)
    horni_znacky_v_punktaci=\
        extract_marks_with_property(MARK_PROP_TOP,punctation_marks_names)
    horni_samohlasky_v_punktaci=\
        extract_marks_with_property(MARK_PROP_TOP_VOWEL,punctation_marks_names)
    horni_akcenty_v_punktaci=\
        extract_marks_with_property(MARK_PROP_TOP_ACCENT,punctation_marks_names)
    samostatne_znacky_v_punktaci=\
        extract_marks_with_property(MARK_PROP_STANDALONE,punctation_marks_names)
    samostatne_samohlasky_v_punktaci=\
        extract_marks_with_property(MARK_PROP_STANDALONE_VOWEL,punctation_marks_names)
    samostatne_akcenty_v_punktaci=\
        extract_marks_with_property(MARK_PROP_STANDALONE_ACCENT,punctation_marks_names)
    zavisle_znacky_v_punktaci=\
        [mark for mark in punctation_marks_names if mark not in STANDALONE_MARKS]
        #filter(lambda zn:zn not in samostatne_znacky,jmena_znacek_punktace)

    # vytvoříme základ: souhláska
    # může mít dageš nebo být koncová (sofít):
    if MARKS["dageš"] in punctation:
        if MARKS["sofít"] in punctation:
            if len(samostatne_samohlasky_v_punktaci)==0:
                result=unicode_hebrew.DAGESH_SOFEET_CONSONANTS[cons_name]
            else:
                result=unicode_hebrew.DAGESH_CONSONANTS[cons_name]
            pridej_mezeru=True
        else:
            result=unicode_hebrew.DAGESH_CONSONANTS[cons_name]
            pridej_mezeru=False
    else:
        if MARKS["sofít"] in punctation:
            if len(samostatne_samohlasky_v_punktaci)==0:
                result=unicode_hebrew.SOFEET_CONSONANTS[cons_name]
            else:
                result=unicode_hebrew.CONSONANTS[cons_name]
            pridej_mezeru=True
        else:
            result=unicode_hebrew.CONSONANTS[cons_name]
            pridej_mezeru=False

    for m in range(len(dolni_samohlasky_v_punktaci)):
        mark = dolni_samohlasky_v_punktaci[m]
        if m==0:
            znacka=unicode_hebrew.MARKS[mark][width]
        else:
            znacka=unicode_hebrew.MARKS[mark][CONSONANT_WIDTH_NARROW]
        result+=znacka
    for mark in dolni_akcenty_v_punktaci:
        if len(dolni_samohlasky_v_punktaci)==0:
            znacka=unicode_hebrew.MARKS[mark][width]
        else:
            if cons_name=="qóf" and mark=="meteg":
                znacka=unicode_hebrew.MARKS[mark][width]
            else:
                znacka=unicode_hebrew.MARKS[mark][CONSONANT_WIDTH_NARROW]
        result+=znacka
    for mark in horni_samohlasky_v_punktaci: #jen chólem
        znacka=unicode_hebrew.MARKS[mark][width]
        result+=znacka
    for mark in horni_akcenty_v_punktaci: #zatím žádný
        znacka=unicode_hebrew.MARKS[mark][width]
        result+=znacka
    # apostrophe musí následovat hned za souhláskou:
    if "apostrophe" in samostatne_akcenty_v_punktaci:
        znacka=unicode_hebrew.MARKS["apostrophe"][width]
        result+=znacka
    for mark in samostatne_samohlasky_v_punktaci:
        znacka=unicode_hebrew.MARKS[mark][width]
        result+=znacka
    for mark in samostatne_akcenty_v_punktaci:
        if mark!="apostrophe": #už jsme ho použili
            znacka=unicode_hebrew.MARKS[mark][width]
            result+=znacka
    if pridej_mezeru:
        result+=' '

    return result

def hebrew(data):
    """hebrew(data: hebrew data)"""
    result = ""
    for group in data:
        result += punctated_consonant(group)
    return result

# These items should each encoding module define

def compile_transcription(transcription):
    hebrew_data = get_hebrew_data(transcription)
    result = hebrew(hebrew_data)
    return result

font = {
    "face":     "serif",
    "size":     14,
    "encoding": wx.FONTENCODING_SYSTEM
}

direction = wx.ALIGN_LEFT

del wx
