# -*- coding: utf8 -*-
"""Španělské kódování pro applikaci Slovíčka"""

import wx
from . import ci

CHARACTERS = {
    u"ň":  u"ñ",
    u"Ň":  u"Ñ",
    }

def Spanish(text):
    "Converts the transcription into Spanish"
    result = compile_simple_transcription(text, CHARACTERS)
    return result

# tyto položky by měl definovat každý dekódovací modul:

def Prevod(transkripce):
    "Převádí transkripci na cílový jazyk"
    vysl = Spanish(transkripce)
    return vysl

Font = {"face": u"serif", "size": 14, "encoding": wx.FONTENCODING_DEFAULT}

Smer = wx.ALIGN_LEFT
