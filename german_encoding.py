# -*- coding: utf8 -*-
"""The German encoding for the Quettar Lambion application"""

import wx
from . import ci

CHARACTERS = {
    'ss':   'ß',
    'ae':   'ä',
    'Ae':   'Ä',
    'AE':   'Ä',
    'oe':   'ö',
    'Oe':   'Ö',
    'OE':   'Ö',
    'ue':   'ü',
    'Ue':   'Ü',
    'UE':   'Ü'
}

def german(text):
    "Converts the transcription into German"
    result = ci.compile_simple_transcription(text, CHARACTERS)
    return result

# These items should each encoding module define:

def compile_transcription(transcription):
    "Converts a transcription into the target tongue"
    result = german(transcription)
    return result

font = {"face": u"serif", "size": 14, "encoding": wx.FONTENCODING_DEFAULT}

direction = wx.ALIGN_LEFT
