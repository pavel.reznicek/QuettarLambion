# -*- coding:utf8 -*-

"""The Lojban encoding for the Quettar Lambion application."""

import sys
import wx

CHARACTERS = {
    "ch":  "x",
    "CH":  "X",
    "h":   "'",
    "H":   "'",
    "ë":   "y",
    "Ë":   "Y",
    r"\e/": "y",
    r"\E/": "Y",
    "ž":   "j",
    "Ž":   "J",
    "š":   "c",
    "Š":   "C"
    }

def lojban(transcription):
    """Compiles the markup text into real Lojban"""
    result = ''
    while transcription:
        for token, char in CHARACTERS.items():
            if transcription.startswith(token):
                result += char
                length = len(token)
                transcription = transcription[length:] # eat the token
                break
        else:
            result += transcription[:1]
            transcription = transcription[1:]
    return result

# These items should each encoding module define:

def compile_transcription(transcription):
    """Public compile function"""
    result = lojban(transcription)
    return result

if sys.platform=='win32':
    font = {
        "face":     "Courier New",
        "size":     14,
        "encoding": wx.FONTENCODING_DEFAULT
    }
else:
    font = {
        "face":     "monospace",
        "size":     14,
        "encoding": wx.FONTENCODING_DEFAULT
    }
direction = wx.ALIGN_LEFT
