#!/usr/bin/env python
#-*- coding: utf8 -*-

"""A module containing helper _c_ommon _i_tems (ci)
for other modules in the Quettar Lambion application"""

import wx

EAT_TOKEN_PRODUCT_SYMBOL = "symbol"
EAT_TOKEN_PRODUCT_REST = "rest"

def swap_dict(dictionary):
    """swap_dict(sl: dict) -> dict

    Creates a new dictionary where the original values
    become keys and vice versa.
    Useful for conversion algorithms.
    """
    result = dict((v, k) for k, v in dictionary.items())
    return result

def nz(value): # pylint: disable=invalid-name
    """Returns always a non-None value.
    For a None value, it returns an empty string,
    otherwise it returns the original value.
    Inspired by the Microsoft Acces NZ (Non-Zero) function."""
    if value is None:
        return ""
    return value

def make_font(fdict):
    """Composes a font out of a specially formed dictionary"""
    #return wx.FFont(fdict['size'],wx.FONTFAMILY_DEFAULT,faceName=fdict['face'],
    #    encoding=fdict['encoding'])
    result = wx.Font(
        pointSize=fdict['size'],
        family=wx.FONTFAMILY_DEFAULT,
        style=wx.FONTSTYLE_NORMAL,
        weight=wx.FONTWEIGHT_NORMAL
    )
    result.FaceName = fdict['face']
    result.Encoding = fdict['encoding']
    return result

def eat_token(text, token_dictionary):
    """Eats the first token and returns its transcription and the rest
of the source text."""
    result = {
        EAT_TOKEN_PRODUCT_SYMBOL: '',
        EAT_TOKEN_PRODUCT_REST: ''
    }
    token_found = False
    for token, symbol in token_dictionary.items():
        if text.startswith(token):
            # Token length
            length = len(token)
            # Add the resulting symbol to the result
            result[EAT_TOKEN_PRODUCT_SYMBOL] = symbol
            # The rest is everything past the token
            result[EAT_TOKEN_PRODUCT_REST] = text[length:]
            token_found = True
            break
    if not token_found: # No token was found in the transcription dictionary
        # Let's eat the first character
        # The resulting symbol is the first character itself
        result[EAT_TOKEN_PRODUCT_SYMBOL] = text[:1]
        # The rest is everything past the first character
        result[EAT_TOKEN_PRODUCT_REST] = text[1:]
    return result

def compile_simple_transcription(text, token_dictionary):
    "Converts the transcription into the actual target language"
    result = ''
    while text:
        product = eat_token(text, token_dictionary)
        symbol = product[EAT_TOKEN_PRODUCT_SYMBOL]
        rest = product[EAT_TOKEN_PRODUCT_REST]
        result += symbol
        text = rest
    return result
