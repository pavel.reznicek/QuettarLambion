# -*- coding:utf8 -*-

"""The Latvian encoding module for the Quettar Lambion application"""

import wx
from . import ci

CHARACTERS={
    u"Á":     0x100,
    u"á":     0x101,
    u"É":     0x112,
    u"é":     0x113,
    u"Ë":     0x118,
    u"ë":     0x119,
    u"Í":     0x12a,
    u"í":     0x12b,
    u"Ú":     0x16a,
    u"ú":     0x16b,
    u"Ď":     0x122,
    u"ď":     0x123,
    u"Ť":     0x136,
    u"ť":     0x137,
    u"Ň":     0x145,
    u"ň":     0x146,
    u"Ľ":     0x13b,
    u"ľ":     0x13c,
    u"´":     0x2c9,
    u"ˇ":     0x326,
}

def latvian(transcription):
    result = ''
    # remove from text step by step:
    while transcription:
        for char in CHARACTERS.keys():
            if transcription.startswith(char):
                length = len(char)
                result += chr(CHARACTERS[char])
                transcription = transcription[length:]
                break
        else:
            result += transcription[0]
            transcription = transcription[1:]
    return result


# These items should each encoding module define:

def compile_transcription(transcription):
    # snad někdy v budoucnu:
    result = latvian(transcription)
    return result

font = {
    "face":     "serif",
    "size":     14,
    "encoding": wx.FONTENCODING_DEFAULT
}

direction = wx.ALIGN_LEFT

del wx
