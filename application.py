#!/usr/bin/env python
#-*- coding:utf8 -*-


"""
Quettar Lambion (Words of Languages)


"""
__author__ = 'Pavel Řezníček'

import wx
import locale
import sys
import os
import codecs
import random
# The "languages" dictionary containing the encoding modules
from .languages import languages, language_names
from . import ci
from .vocable_edit_dialog import DlgVocableEdit
from cigydd import Pyp

CESTA = os.path.dirname(__file__)

def vocabulary_file_name(name_base):
    """The whole path to the vocabulary file"""
    return os.path.join(CESTA, name_base + '.txt')

def load_vocable_list(language):
    """Loads a raw vocable list from a vocabulary file"""
    try:
        with open(vocabulary_file_name(language), 'rb+', 1) as voc_file:
            file_contents = voc_file.read() # file → string
    except IOError:
        file_contents = ''
    # Remove the trailing line endings
    result = file_contents.splitlines(keepends=False)
    return result

def build_vocable_tuple(native, foreign, test):
    """Builds up the vocable tuple from the elemnts given"""
    result = (native, (test, foreign))
    return result

def split_vocable_line(string):
    """Splits a raw vocable line into language terms"""
    if isinstance(string, bytes):
        line = string.decode('utf-8').split(u'\t')
    else:
        line = string.split(u'\t')
    # text file format:
    # "test\tNative\tForeign" where test is in ('0', '1')
    if len(line) == 0:
        result = None
    else:
        if len(line) == 1:
            native = line[0]
            foreign = ''
            test = False
        elif len(line) == 2:
            if line[0] in ('0', '1'):
                native = line[1]
                foreign = ''
                test = bool(int(line[0]))
            else:
                native = line[0]
                foreign = line[1]
                test = False
        else: #>=3
            native = line[1]
            foreign = line[2]
            test = bool(int(line[0]))
        result = build_vocable_tuple(native, foreign, test)
    return result


def load_vocabulary(language):
    """Loads a vocabulary from a file, trying to repair malformed entries"""
    voc_list = load_vocable_list(language)
    raw_vocabulary = [split_vocable_line(s) for s in voc_list]
    # Wipe out the None values – empty lines
    vocabulary = [voc for voc in raw_vocabulary if voc is not None]
    return dict(vocabulary)


def save_vocabulary(language, vocabulary):
    """Saves a vocabulary to a file"""
    def junction(tup):
        native = tup[0]
        test = tup[1][0]
        foreign = tup[1][1]
        if test:
            test_value = 1
        else:
            test_value = 0
        result = '%s\t%s\t%s'%(test_value, native, foreign)
        return result

    if vocabulary: # it could happen that the vocabulary wouldn’t be loaded
        lines = sorted(junction(sl) for sl in vocabulary.items())
        filetext = os.linesep.join(lines)
        with open(vocabulary_file_name(language), 'wb+', 1) as vocfile:
            vocfile.write(filetext.encode('utf-8'))

class AppQuettarLambion(wx.App):
    """The main application"""
    def OnInit(self): # pylint: disable=invalid-name
        """Initialization"""
        fra = QuettarWindow()
        self.SetTopWindow(fra)
        if fra.Show() != 0:
            return True

class QuettarWindow(wx.Frame): # pylint: disable=too-few-public-methods
    """The main window"""
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "Quettar Lambion")

        # nevytvářet StatusBar jako =wx.StatusBar(...),
        # protože pak rozhází hlavní rozměrník (nebo se za něj schová);
        # místo toho =self.CreateStatusBar().
        ##status=self.status=self.CreateStatusBar()

        self.vocabulary = None
        self.vocabulary_name = ''

        szr_all = self.szr_all = wx.BoxSizer(wx.VERTICAL)

        notebook = self.notebook = wx.Notebook(self, -1)
        szr_all.Add(notebook, 1, wx.GROW)

        tab_list = self.tab_list = ListTab(
            notebook, self.vocabulary_name, self.vocabulary
        )
        notebook.AddPage(tab_list, u'Vocables')

        tab_testing = self.tab_testing = TestingTab(notebook)
        notebook.AddPage(tab_testing, u'Testing')


##        # Takhle ne!!!!!!!!!!!!!!!!
##        # Způsobuje to systémovou chybu!!
##        # (A NotebookSizer je nejspíš přiřazen
##        #     už při svém vytvoření.)
##        nb.SetSizer(szrNB)
##        szrNB.SetSizeHints(nb)


        self.SetSizer(szr_all)
        szr_all.SetSizeHints(self)

##        # I tohle způsobuje chybu
##        # a obejdeme se bez toho.
##        self.SetSizer(szrNB)
##        szrNB.SetSizeHints(self)

        self.CentreOnScreen()

        self.Bind(wx.EVT_CLOSE, self.on_close)

    def on_close(self, evt):
        """The form close event"""
        save_vocabulary(
            self.tab_list.cb_vocabulary.GetStringSelection(), self.vocabulary
        )
        evt.Skip()

class ListTab(wx.Panel):
    """A notebook tab containing the vocable list overview
    & editing functions"""
    def __init__(self, parent, vocabulary_name, vocabulary):
        wx.Panel.__init__(self, parent, -1)

        self.vocabulary_name = vocabulary_name
        self.vocabulary = vocabulary

        szr_all = wx.BoxSizer(wx.VERTICAL)

        cb_vocabulary = self.cb_vocabulary = VocabularyCombo(self)
        szr_all.Add(cb_vocabulary, 0, wx.GROW | wx.LEFT | wx.TOP | wx.RIGHT, 8)

        szr_list_and_buttons = wx.BoxSizer(wx.HORIZONTAL)

        szr_list = wx.BoxSizer(wx.VERTICAL)
        clb_vocables = self.clb_vocables = wx.CheckListBox(
            self, -1,
            style=wx.LB_SORT | wx.LB_SINGLE | wx.LB_ALWAYS_SB,
            size=(200, 200)
        )
        szr_list.Add(clb_vocables, 1, wx.GROW | wx.ALL, 8)
        szr_list_and_buttons.Add(szr_list, 1, wx.GROW)

        pan_buttons = ButtonPanel(self)
        szr_list_and_buttons.Add(
            pan_buttons, 0,
            wx.RIGHT | wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER_VERTICAL, 6
        )

        szr_all.Add(szr_list_and_buttons, 1, wx.GROW)

        self.SetSizer(szr_all)
        szr_all.SetSizeHints(self)

        cb_vocabulary.Bind(wx.EVT_CHOICE, self.cb_vocabulary_select)
        cb_vocabulary.Select(0) # behold, this doesn’t raise the event
                                # so we simulate it:
        self.cb_vocabulary_select(None)

        self.clb_vocables.Bind(wx.EVT_CHECKLISTBOX, self.clb_vocables_check)

        btn_new = pan_buttons.btn_new
        btn_new.Bind(wx.EVT_BUTTON, self.btn_new_click)
        btn_edit = pan_buttons.btn_edit
        btn_edit.Bind(wx.EVT_BUTTON, self.btn_edit_click)
        btn_delete = pan_buttons.btn_delete
        btn_delete.Bind(wx.EVT_BUTTON, self.btn_delete_click)
        btn_save_vocabulary = pan_buttons.btn_save_vocabulary
        btn_save_vocabulary.Bind(wx.EVT_BUTTON, self.btn_save_vocabulary_click)

    def btn_new_click(self, evt):
        new_voc = self.new_vocable()
        if new_voc != None:
            while new_voc[0] in self.vocabulary: # to avoid an overwrite
                new_voc[0] += '*' # add asterisks
            self.clb_vocables.Append(new_voc[0]) # gets in the right place
            self.clb_vocables.SetStringSelection(new_voc[0]) # select it
            # add the new vocable to the vocabulary
            self.vocabulary.update({new_voc[0]: new_voc[1]})
            self.clb_vocables.Check(
                self.clb_vocables.GetSelection(),
                new_voc[1][0]
            )
            evt.Skip()

    def btn_edit_click(self, evt):
        edited_voc = self.vocable_editation()
        if edited_voc != None:
            # remove the original vocable form from the vocabulary
            self.vocabulary.pop(self.clb_vocables.GetStringSelection(), None)
            self.clb_vocables.SetString(
                self.clb_vocables.GetSelection(),
                edited_voc[0]
            )
            # return the new vocable form into the vocabulary
            self.vocabulary.update({edited_voc[0]: edited_voc[1]})
            self.clb_vocables.Check(
                self.clb_vocables.GetSelection(),
                edited_voc[1][0]
            )
        evt.Skip()

    def btn_delete_click(self, evt):
        sez = self.clb_vocables
        sl = sez.GetStringSelection()
        if sl:
            odp = wx.MessageBox(
                u"Opravdu chcete smazat slovíčko „%s“?"%sl,
                u"Smazání slovíčka", wx.YES_NO|wx.ICON_QUESTION, self
                )
            if odp == wx.YES:
                self.vocabulary.pop(sl, '')
                sez.Delete(sez.GetSelection())
        evt.Skip()

    def btn_save_vocabulary_click(self, evt):
        if self.vocabulary and self.vocabulary_name:
            # A vocabulary is loaded now -> save it
            save_vocabulary(self.vocabulary_name, self.vocabulary)
        else:
            wx.MessageBox(
                "Select a vocabulary first.",
                "Vocabulary Saving", wx.ICON_INFORMATION, self
                )
        evt.Skip()

    def cb_vocabulary_select(self, evt):
        if self.vocabulary and self.vocabulary_name:
            # A vocabulary is loaded now -> save it
            save_vocabulary(self.vocabulary_name, self.vocabulary)
        self.vocabulary_name = self.selected_vocabulary
        self.vocabulary = load_vocabulary(self.vocabulary_name)
        self.update_list()
        if evt:
            evt.Skip()

    def update_list(self):
        clb_vocables = self.clb_vocables
        clb_vocables.Clear()
        clb_vocables.AppendItems(list(self.vocabulary.keys()))
        # Checking the checkboxes:
        for it in range(clb_vocables.GetCount()):
            native = clb_vocables.GetString(it)
            check = self.vocabulary[native][0]
            clb_vocables.Check(it, check)

    def clb_vocables_check(self, evt):
        sel = evt.GetSelection()
        native = self.clb_vocables.GetString(sel)
        foreign = self.vocabulary[native][1]
        test = self.clb_vocables.IsChecked(sel)
        if sel > -1:
            if native in self.vocabulary.keys():
                self.vocabulary.update({native: (test, foreign)})
        evt.Skip()

    @property
    def selected_vocable(self):
        selection = self.clb_vocables.GetStringSelection()
        if selection != '':
            return selection
        else:
            return None

    @property
    def selected_vocabulary(self):
        return self.cb_vocabulary.selected_vocabulary

    def new_vocable(self):
        dlg=DlgVocableEdit(self, self.selected_vocabulary, None)
        vysl=dlg.ShowModal()
        if vysl==wx.ID_OK:
            return dlg.vocable
        else:
            return None

    def vocable_editation(self):
        native = self.selected_vocable
        if native != None:
            selvoc = [native, self.vocabulary[native]]
        else:
            selvoc = None
        if selvoc != None:
            dlg = DlgVocableEdit(self, self.selected_vocabulary, selvoc)
            response = dlg.ShowModal()
        else:
            wx.MessageBox(u"Nevybral(a) jste žádné slovíčko!",
                u"Úprava slovíčka",wx.ICON_WARNING)
            response = None
        if response == wx.ID_OK:
            return dlg.vocable
        else:
            return None


class ButtonPanel(wx.Panel):
    def __init__(self,parent):
        wx.Panel.__init__(self,parent,-1)
        szrTl=self.szrTl=wx.BoxSizer(wx.VERTICAL)

        self.SetSizer(szrTl)

        btn_new = self.btn_new = wx.Button(self, -1, "&New vocable")
        btn_edit = self.btn_edit = wx.Button(self, -1, "&Edit vocable")
        btn_delete = self.btn_delete = wx.Button(self, -1, "&Delete vocable")
        btn_save_vocabulary = self.btn_save_vocabulary = \
            wx.Button(self, -1, "&Save vocabulary")

        szrTl.Add(btn_new, 1, wx.GROW)
        szrTl.Add((8,8))
        szrTl.Add(btn_edit, 1, wx.GROW)
        szrTl.Add((8,8))
        szrTl.Add(btn_delete, 1, wx.GROW)
        szrTl.Add((8,8), 1, wx.GROW)
        szrTl.Add(btn_save_vocabulary, 1, wx.GROW)

        szrTl.SetSizeHints(self)

class TestingTab(wx.Panel):
    def __init__(self,parent):
        wx.Panel.__init__(self,parent,-1)

        self.testing_vocabulary_name = None
        self.testing_vocabulary = None
        self.order = None

        self.fn_compile_transcritpion = None

        szr_all=wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(szr_all)

        btn_start=wx.Button(self, label="&Start")
        szr_all.Add(btn_start, 0, wx.GROW | wx.ALL, 8)
        btn_start.Bind(wx.EVT_BUTTON, self.btn_start_click)

        self.dlg_testing = DlgVocabularySelection(self)

        lbl_foreign = self.lbl_foreign = wx.TextCtrl(self, style=wx.TE_READONLY)
        szr_all.Add(lbl_foreign, 0, wx.GROW | wx.LEFT | wx.RIGHT | wx.BOTTOM, 8)

        # A debug switch:
        special_list_for_windows = False

        if wx.Platform == '__WXMSW__' and special_list_for_windows:
            pan_list = wx.Panel(self)
            pan_list.szr = wx.BoxSizer(wx.VERTICAL)
            lst_vocables = wx.ComboBox(
                pan_list, style=wx.CB_SORT | wx.CB_SIMPLE
            )
            pan_list.szr.Add(lst_vocables, 1, wx.GROW)
            lst_vocables.Bind(wx.EVT_LEFT_DCLICK, self.lst_vocables_dbl_click)
            lst_vocables.Bind(wx.EVT_KEY_DOWN,self.lst_vocables_char)
            szr_all.Add(pan_list, 1, wx.GROW | wx.LEFT | wx.RIGHT, 8)
        else:
            lst_vocables = wx.ListBox(self, style = wx.LB_SORT | wx.LB_SINGLE)
            lst_vocables.Bind(
                wx.EVT_LISTBOX_DCLICK, self.lst_vocables_dbl_click
            )
            szr_all.Add(lst_vocables, 1, wx.GROW | wx.LEFT | wx.RIGHT, 8)
        self.lst_vocables = lst_vocables

        ukUspech = self.pb_success = wx.Gauge(
            self, style=wx.GA_HORIZONTAL | wx.GA_SMOOTH
        )
        szr_all.Add(ukUspech, 0, wx.GROW | wx.ALL, 8)
        self.Layout()

    def btn_start_click(self,evt):
        self.start()
        evt.Skip()

    def lst_vocables_char(self,evt):
        if evt.GetUniChar()==13:
            self.check()
            evt.Skip()
        else:
            evt.Skip()


    def lst_vocables_dbl_click(self,evt):
        """The clb_vocables event handler"""
        self.check()
        evt.Skip()

    def start(self):
        really_start = True
        if self.order:
            response = wx.MessageBox("A test is currently in progress. "
                "Do you want to stop and get tested anew?",
                "Start testing?", wx.YES_NO | wx.ICON_WARNING)
            if response != wx.YES:
                really_start = False
        if really_start:
            result = self.dlg_testing.ShowModal()
            if result == wx.ID_OK:
                testing_vocabulary_name = self.testing_vocabulary_name = \
                    self.dlg_testing.vocabulary_name
                testing_vocabulary = self.testing_vocabulary = \
                    load_vocabulary(testing_vocabulary_name)
                encoding_module = languages[testing_vocabulary_name]

                fnt_native = wx.FFont(14, wx.FONTFAMILY_ROMAN)

                if hasattr(encoding_module, 'font'):
                    fnt_foreign=ci.make_font(encoding_module.font)
                else:
                    fnt_foreign=fnt_native
                if hasattr(encoding_module, 'direction'):
                    direction = encoding_module.direction
                else:
                    direction = wx.ALIGN_LEFT
                if hasattr(encoding_module,'compile_transcription'):
                    self.fn_compile_transcritpion = \
                        encoding_module.compile_transcription
                else:
                    self.fn_compile_transcritpion = lambda s: s

                self.lbl_foreign.SetFont(fnt_foreign)
                self.lbl_foreign.SetWindowStyle(direction)

                self.Layout()

                # select vocables marked for testing
                order = [
                    key for key in testing_vocabulary.keys()
                    if testing_vocabulary[key][0]
                ]
                # shuffle
                random.shuffle(order)
                self.order = order # publish
                self.lst_vocables.Clear()
                self.lst_vocables.AppendItems(order) # fill the list control
                self.pb_success.SetRange(len(order)) # set the progress bar range
                self.pb_success.SetValue(0) # reset the progress bar to zero
                self.test() # and go!

    def test(self):
        if self.order: # there is yet something to test for:
            native = self.order[0]
            transcription = self.testing_vocabulary[native][1]
            foreign = self.fn_compile_transcritpion(transcription)
            self.lbl_foreign.SetValue(foreign)

    def check(self):
        native = self.lst_vocables.GetStringSelection()
        if self.order:
            if native == self.order[0]:
                got_it_right = True
            else:
                got_it_right = False

            if got_it_right:
                self.pb_success.SetValue(self.pb_success.GetValue() + 1)
                # If the evaluation won’t follow then do a “Pyp”
                if len(self.order) > 1:
                    Pyp()
            else:
                wx.MessageBox(
                    "You chose: %s\nThe right choice "
                    "is: %s"%(native,self.order[0]),
                    "Mistake", wx.OK | wx.ICON_WARNING, self
                )

            # got tested for this vocable – remove it from the list
            self.order.pop(0)

        if not self.order: # nothing remains:
            right_guesses = self.pb_success.GetValue()
            total = self.pb_success.GetRange()
            percent = (float(right_guesses) / float(total)) * 100
            wx.MessageBox(
                'You have just been tested for all the vocables in the „%s“ '
                'vocabulary.\n'
                '%s%% was right (%s of the total %s vocables).'
                %(self.testing_vocabulary_name, percent, right_guesses, total),
                "Evaluation", wx.ICON_INFORMATION | wx.OK, self)
            self.lst_vocables.Clear()
            self.lbl_foreign.SetValue(u'')
            self.pb_success.Value = 0
        else: # something remains
            # go on
            self.test()


class DlgVocabularySelection(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, title="Testing")
        szr_all = self.szr_all = wx.BoxSizer(wx.VERTICAL)

        szr_buttons = self.CreateButtonSizer(wx.OK | wx.CANCEL)

        cb_language = self.cb_language = VocabularyCombo(self)
        szr_all.Add(cb_language, 0, wx.GROW | wx.ALL, 8)

        szr_all.Add(szr_buttons,0,wx.GROW|wx.ALL,8)

        self.SetSizer(szr_all)
        self.SetClientSize(szr_all.GetMinSize())

    @property
    def vocabulary_name(self):
        result = self.cb_language.selected_vocabulary
        return result

class VocabularyCombo(wx.Choice):
    def __init__(self,parent):
        wx.Choice.__init__(self, parent)
        self.AppendItems(language_names)
        if self.GetCount() > 0:
            self.Select(0)

    @property
    def selected_vocabulary(self):
        return self.GetStringSelection()

def run():
    app=AppQuettarLambion(redirect=False)
    app.MainLoop()

if __name__ == "__main__":
    run()
