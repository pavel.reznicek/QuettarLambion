# -*- coding: utf8 -*-

"""The Unicode Hebrew character maps for the Quettar Lambion application"""

from . import ci

CONSONANTS = {
    "álef"              : chr(0x05D0),
    "bét"               : chr(0x05D1),
    "gímel"             : chr(0x05D2),
    "dálet"             : chr(0x05D3),
    "hé"                : chr(0x05D4),
    "wáw"               : chr(0x05D5),
    "zajin"             : chr(0x05D6),
    "chét"              : chr(0x05D7),
    "tét"               : chr(0x05D8),
    "jód"               : chr(0x05D9),
    "kaf"               : chr(0x05DB),
    "lámed"             : chr(0x05DC),
    "mém"               : chr(0x05DE),
    "nún"               : chr(0x05E0),
    "sámek"             : chr(0x05E1),
    "ajin"              : chr(0x05E2),
    "pé"                : chr(0x05E4),
    "cáde"              : chr(0x05E6),
    "qóf"               : chr(0x05E7),
    "réš"               : chr(0x05E8),
    "sín"               : chr(0x05E9) + chr(0x05C2),
    "šín"               : chr(0x05E9) + chr(0x05C1),
    "táw"               : chr(0x05EA),
    "mater lectionis"   : chr(0x001F)
}
CONSONANT_NAMES = ci.swap_dict(CONSONANTS)

DAGESH_CONSONANTS = CONSONANTS.copy()
for C in DAGESH_CONSONANTS:
    DAGESH_CONSONANTS[C] += chr(0x05BC)
DAGESH_CONSONANT_NAMES = ci.swap_dict(DAGESH_CONSONANTS)

SOFEET_CONSONANTS = CONSONANTS.copy()
SOFEET_CONSONANTS_UPDATE = {
    "kaf"       : chr(0x05DA),
    "mém"       : chr(0x05DD),
    "nún"       : chr(0x05DF),
    "pé"        : chr(0x05E3),
    "cáde"      : chr(0x05E5)
}
SOFEET_CONSONANTS.update(SOFEET_CONSONANTS_UPDATE)
SOFEET_CONSONANTS_NAMES = ci.swap_dict(SOFEET_CONSONANTS)

DAGESH_SOFEET_CONSONANTS = DAGESH_CONSONANTS.copy()
DAGESH_SOFEET_CONSONANTS_UPDATE = SOFEET_CONSONANTS_UPDATE.copy()
for C in DAGESH_SOFEET_CONSONANTS_UPDATE:
    DAGESH_SOFEET_CONSONANTS_UPDATE[C] += chr(0x05BC)
DAGESH_SOFEET_CONSONANTS.update(DAGESH_SOFEET_CONSONANTS_UPDATE)
DAGESH_SOFEET_CONSONANT_NAMES = ci.swap_dict(DAGESH_SOFEET_CONSONANTS)

MARKS = {
    # by consonant width:
    #                  narrow,      normal,      qof,         lamed
    "dageš"         : (None,       ) * 4,
    "šewa"          : (chr(0x05B0),) * 4,
    "patach"        : (chr(0x05B7),) * 4,
    "chatef patach" : (chr(0x05B2),) * 4,
    "qámec"         : (chr(0x05B8),) * 4,
    "chatef qámec"  : (chr(0x05B3),) * 4,
    "segól"         : (chr(0x05B6),) * 4,
    "chatef segól"  : (chr(0x05B1),) * 4,
    "céré"          : (chr(0x05B5),) * 4,
    "chíreq"        : (chr(0x05B4),) * 4,
    "chólem"        : (chr(0x05BA), chr(0x05B9), chr(0x05B9), chr(0x05B9)),
    "chólem gadól"  : (chr(0x05D5) + chr(0x05BA),) * 4,
    "qibbúc"        : (chr(0x05BB),) * 4,
    "šúreq"         : (chr(0x05D5) + chr(0x05BC),) * 4,
    "sofít"         : (None,       ) * 4,
    "meteg"         : (chr(0x05BD),) * 4,
    "atnách"        : (chr(0x0591),) * 4,
    "sillúq"        : (chr(0x05C3),) * 4,
    "sóf pásúq"     : (chr(0x05C3),) * 4,
    "maqef"         : (chr(0x05BE),) * 4,
    "apostrof"      : (chr(0x05F3),) * 4,
    "unknown mark"  : (chr(0xE000),) * 4
}
