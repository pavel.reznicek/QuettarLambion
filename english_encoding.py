# -*- coding: utf8 -*-
"""The English encoding for the Quettar Lambion application"""

import wx
from . import ci

CHARACTERS = {
    }

def English(transcription):
    "Converts the transcription into English"
    result = ci.compile_simple_transcription(transcription, CHARACTERS)
    return result

# Following items should each encoding module define:

def compile_transcription(transcription):
    "Transforms the transcription given into the target language"
    result = English(transcription)
    return result

font = {
    "face":     "serif",
    "size":     14,
    "encoding": wx.FONTENCODING_DEFAULT
}

direction = wx.ALIGN_LEFT
