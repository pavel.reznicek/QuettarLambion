# Quettar Lambion
## Words of Languages

A vocabulary for several languages with the ability to edit and add more 
language modules.

Editing for each language is done through a transcription system that can be 
defined by the user so you don’t have to install an additional keyboard layout.

You can test your knowledge of the vocabularies you defined.
