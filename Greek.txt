0	buď zdráv	cha'ire!
1	bouřlivé a deštivé počasí; roční období takového počasí = „zima“	‘o cheim'ón, ~ónoc
1	buditi, probouzeti; vztyčovati (též ve významu: budovati); pass.: probouzeti se; vstávati, vstáti (i z mrtvých); pozvednouti se, vystoupiti	’ege'iró, ein / ’ege'iromai, es\th/ai \(/’'égeira, ’eg'égermai, ’eg'er\th/én\)/
1	běda	o)ua'i
1	cizí, římský; Říman	(rom~aioc, a, on
1	dům, obydlí, byt, jizba, síň, střecha domu; rod	t`o d~óma, d'ómatoc \(/\?/\)/
1	hora, pohoří	t`o )'oroc: t`a )'oré
1	hrůza, ohavnost (to, co působí ošklivost)	t`o bd'elygma, atoc
1	jaký, jaké kvality (vztažné zájmeno)	o('ioc, a, on
1	jestliže, -li; při přísaze: jistě ne!; v otázkách: zda	e’i
1	jestliže, -li; částice spojená se slovesy v podmínečných a pod. větách	)'an
1	komoliti, krátiti	kolob~ó, o~un
1	lupič	‘o l'é|stéc
1	modliti se k někomu, někoho prositi (vzývati), chválou zahrnovati	prose'uchomai, esthai \(/proséux'amén\)/
1	někdy, jindy	tot'e
1	obrátiti, otočiti, vésti zpět, k návratu nutiti; intr. (med.) obrátiti se, vrátiti se, navštíviti, dbáti, pobývati, probíhati	)epistr'e\f/ó, ein
1	pochopitelný (pochopený) rozumem (smysly)	noét'oc, 'é, 'on
1	poznávati (zcela, dobře); předčítati, čísti, přemluviti; uznávati	)anagign'óskó, ein
1	předem říkati; předpovídati	prol'egó, ein \(/proe~ipa, proe~ipon, proe'iréka\)/
1	sem; zde	‘~óde
1	silný, mocný	dynat'oc, 'é, 'on
1	ssáti, kojiti	\th/él'azó, ein
1	takže; pročež; tedy	‘'óste
1	tehdy, tenkrát, teprve; dříve, předtím, pak, potom, nato	t'ot’ , t'ote
1	utíkati, prchati, uniknouti, vyhýbati se, varovati se, zmizeti, býti vypovězen, ve vyhnanství žíti, býti pohnán před soud	\f/e'ugó, ein
1	uváděti v omyl, sváděti se správné cesty; klamati, sváděti, blouditi, mýliti se; propadnouti bludu; býti podveden, sveden	plan~ó, ~an \(/’epl'anésa, pepl'anémai, ’eplan'é\th/én\)/
1	vybrati si, zvoliti (si), vyvoliti	’ekl'egomai, es\th/ai \(/’exelex'amén, ’ekl'elegmai\)/
1	zdvihati, velebiti, vzdáliti, opatřiti, odstraniti, odejmouti, odtrhnouti, vzíti; intr. zdvihnouti se, vyplouti; pas. vystupovati; med. dostati, obdržeti, převzíti, podniknouti	a)'iró, ein \(/)ar~ó, )~éra, )~érka, )~érmai, )~ér\th/én\)/
1	zloděj	(o kl'eptéc
1	zpustošení, zničení	(é )er'émósic, eóc
1	zásobní komora, zásobárna; kasema; vůbec vnitřní, skrytá, soukromá místnost: pokojík; hebr. cheder	t`o tamie~ion \(/tame~ion\)/, ou
1	zázrak, div; pův.: věštné znamení, potom: mimořádný úkaz	t`o t'erac, atoc
1	zářící, bílý; nečinný, líný, nezpracovaný, neschopný, zanedbaný, neprospěšný	)arg'oc, 'é, 'on
1	útěk	‘é \f/yg'é, ~éc
1	šat, oděv, plášť, pokrývka, plátěný pytlík	t`o (im'ation \(/( _imation\)/, ou