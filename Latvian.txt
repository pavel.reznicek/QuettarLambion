0		sveiki
0	Ainaži, město na severu Lotyšska	Ainaži
0	Kuiviži, vesnice u moře	Kuiviži
0	Na shledanou!	Uz redzéšanos!
0	Pēterupe, potok v Saulkrastech	Péterupe
0	církev, kostel	bazníca
0	hrobník	Ainárs
0	jméno farářovic dcerky	Asnáte
0	kvas (alkoholický nápoj)	kvass
0	země	zeme
0	číslo, číslovka	numurs
1	Dobrý den.	Labdien.
1	Děkuji.	Paldies.
1	Na viděnou! (Na shledanou!)	Uz redzéšanoš!
1	Němec	Vácis
1	Prodám zemi (pozemek)	Párdod zemi
1	Prosím.	Lúdzu.
1	Rus	Krievis
1	Slunobřehy	Saulkrasti
1	Vše dobré! (Sbohem!)	Visu labu!
1	celostátní, celozemský	viszemáks
1	cena	cena
1	chlebová polévka (kaše)	maizes zupa
1	chléb	maize
1	cukr	cukurs
1	cvičit, procvičovat	vingrinát
1	den	diena
1	dva do Rígy	divas uz Rígu
1	dům	mája
1	fara	mácítájmuiža
1	farář	mácítájs
1	hotel	viesníca
1	hrad, zámek	pils
1	jméno paní farářové	Solvita
1	jméno zahradníka	Jánis
1	kostel, církev	bazníca
1	kvetoucí	ziedošs
1	maják	báka
1	maso	gaľa
1	medvěd	krastiňš
1	mléko	piens
1	moře	júra
1	město	pilséta
1	nebe, obloha	debess
1	něco mezi kavárnou a restaurací	kafejníca
1	obchod	veikals
1	olej	eľľa
1	pivo	alus
1	pohanka	griťi
1	pohankové vločky	griťu párslas
1	polévka	zupa
1	příští zastávka	nákamá pietura
1	půlečka	pusíte
1	smetana	kréjums
1	strom	koks
1	trh	tirgus
1	třešeň	ťirš
1	ulice	iela
1	voda	údens
1	východ	izeja
1	výprodej	izpárdošana
1	včela	bite
1	škola	skola
1	šprot	šprots
1	Železničářská ulice	Dzelsceľnieku iela
1	železnice	dzelsceľš