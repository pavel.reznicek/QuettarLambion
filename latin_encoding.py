# -*- coding:utf8 -*-

"""The Latin encoding module for the Quettar Lambion application"""

import wx
from . import ci

CHARACTERS = {
    u"Á":    0x0100,
    u"á":    0x0101,
    u"Ă":    0x0102,
    u"ă":    0x0103,
    u"˘A":   0x0102,
    u"˘a":   0x0103,
    u"AE":   0x00c6,
    u"Ae":   0x00c6,
    u"ae":   0x00e6,
    u"É":    0x0112,
    u"é":    0x0113,
    u"˘E":   0x0114,
    u"˘e":   0x0115,
    u"Í":    0x012a,
    u"í":    0x012b,
    u"˘I":   0x012c,
    u"˘i":   0x012d,
    u"Ó":    0x014c,
    u"ó":    0x014d,
    u"˘O":   0x014e,
    u"˘o":   0x014f,
    u"OE":   0x0152,
    u"Oe":   0x0152,
    u"oe":   0x0153,
    u"Ú":    0x016a,
    u"ú":    0x016b,
    u"˘U":   0x016c,
    u"˘u":   0x016d,
    u"Ý":    0x0232,
    u"ý":    0x0233
}

def latin(transcription):
    result = ''
    # remove from text step by step:
    while transcription:
        for char in CHARACTERS.keys():
            if transcription.startswith(char):
                length = len(char)
                code = CHARACTERS[char]
                result += chr(code)
                transcription = transcription[length:]
                break
        else:
            result += transcription[0]
            transcription = transcription[1:]
    return result


# These items should each encoding module define:

def compile_transcription(transcription):
    result = latin(transcription)
    return result

font = {
    "face":     u"serif",
    "size":     14,
    "encoding": wx.FONTENCODING_DEFAULT
}

direction = wx.ALIGN_LEFT

del wx
