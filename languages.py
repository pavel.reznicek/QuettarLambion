#!/usr/bin/env python
# -*- coding: utf8 -*-

u'''
Modul Jazyky. Obsahuje definici všech dostupných jazyků.
Tato definice je uložena v pythonovém slovníku "Jazyky".
Zde jsou hodnotami přímo kódovací moduly pro jednotlivé jazyky.
Každému jazyku by měl odpovídat jeden příslušný soubor se slovíčky
se stejným jménem, jako jméno jazyka zde uvedené.
'''

from . import hebrew_encoding
from . import greek_encoding
from . import latin_encoding
from . import lojban_encoding
from . import latvian_encoding
from . import spanish_encoding
from . import german_encoding
from . import english_encoding

languages = {
    u"Hebrew":  hebrew_encoding,
    u"Greek":   greek_encoding,
    u"Latin":   latin_encoding,
    u"Lojban":  lojban_encoding,
    u"Latvian": latvian_encoding,
    u"Spanish": spanish_encoding,
    u"German":  german_encoding,
    u"English": english_encoding
    }

language_names = sorted(languages.keys())
